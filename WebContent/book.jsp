<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	

<style>
.content-header {
	font-family: 'Oleo Script', cursive;
	color: white;
	font-size: 45px;
}

.section-content {
	text-align: center;
}

ul#navigation {
	position: fixed;
	margin: 0px;
	padding: 0px;
	top: 150px;
	left: 50px;
	list-style: none;
	z-index: 9999;
}

span {
	font-size: 1.3em !important;
}

span i {
	font-size: 1.5em !important;
	padding-top: 25px !important;
}

ul#navigation li {
	width: 100px;
}

ul#navigation li a {
	display: block;
	margin-left: -2px;
	width: 300px;
	height: 100px;
	color: white;
	background-color: inherit;
	background-repeat: no-repeat;
	background-position: center center;
	border: 2px solid;
	-moz-border-radius: 0px 10px 10px 0px;
	-webkit-border-bottom-right-radius: 10px;
	-webkit-border-top-right-radius: 10px;
	-khtml-border-bottom-right-radius: 10px;
	-khtml-border-top-right-radius: 10px;
	/*-moz-box-shadow: 0px 4px 3px #000;
    -webkit-box-shadow: 0px 4px 3px #000;
    */
	opacity: 0.6;
	filter: progid:DXImageTransform.Microsoft.Alpha(opacity=60);
}
</style>

</head>
<body style="background: linear-gradient(to left, #3a6186, black);">

<jsp:include page="loginheader.jsp"/>
	<script>
$(function() {
    $('#navigation a').stop().animate({'marginLeft':'-55px'},1000);

    $('#navigation > li').hover(
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-2px'},200);
        },
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-55px'},200);
        }
    );
});

</script>
<h1 id="h11" class="content-header wow fadeIn " style="text-align: center; margin-top:100px; "><div id="notificationMsg"></div></h1>

<div class="container">
<div class="row">

<div class="form-group">
					<div class="row">
	<label for="carnumber" class="col-sm-2 control-label"
		style="color: white;">Car Number</label>
	<div class="col-sm-3">
		<input type="text" class="form-control" name="carnumber"
											id="carnumber" placeholder="Car Number" />
										
									</div></div>
								</div>
								
								<div class="form-group">
					<div class="row">
	<label for="date" class="col-sm-2 control-label"
		style="color: white;">Starting Date</label>
	<div class="col-sm-3">
		<input type="date" class="form-control" name="startingdate"
											id="startingdate" placeholder="Strting Date" />
										
									</div></div>
								</div>
								
							<div class="form-group">
					<div class="row">
	<label for="date" class="col-sm-2 control-label"
		style="color: white;">Ending Date</label>
	<div class="col-sm-3">
		<input type="date" class="form-control" name="endingdate"
											id="endingdate" placeholder="Ending Date" />
										
									</div></div>
								</div>
								
								
								<div class="row">
									<div class="col-sm-2"></div>
									<div class="col-sm-10">
										<button type="submit" class="btn btn-primary btn-sm"
											id="">Submit</button>

									</div>
								</div>	
								


</div>
</div>
</body>
</html>