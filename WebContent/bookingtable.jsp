<%@page import="sun.security.x509.AVA"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.bytewheels.bean.AvailableCar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
	<script src="javascript/bookedbikedetails.js"></script>
</head>
<body>

<div id="bookedBikeMessage" style="text-align: center;color:white"></div>
    <h1 style="text-align: center; color: white;">Available car's </h1>
    
    <%
    if(request.getAttribute("availablecar")!=null){
    int i=0;
    List list=(List)request.getAttribute("availablecar"); 
    Iterator iterator=list.iterator();
    
    if(iterator.hasNext()){%>
    <table class="table table-dark ">
    	  <thead >
    	    <tr>
    	      <th scope="col" style="color: white;">#</th>
    	      <th scope="col" style="color: white;">#</th>
    	      <th scope="col" style="color: white;">Car Category</th>
    	      <th scope="col" style="color: white;">Car Name</th>
    	      <th scope="col" style="color: white;">Car Number</th>
    	      <th scope="col" style="color: white;">Price</th>
    	    </tr>
    	  </thead>
    	  <tbody>
    	  
    	   <% 
    while(iterator.hasNext()){
    	i++;
    AvailableCar avcar=	(AvailableCar)iterator.next();
  %>
    	
    	 
    <tr>
   <th scope="row" style="color: white;"> <input type="radio" id="bookcar"  name="bookcar" value="<%  out.print(avcar.getCarnumber());  %>" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"/></th>
    <th scope="row" style="color: white;"><%= i%></th>
    <td style="color: white;"><%=avcar.getCarcategory()%></td>
 <td style="color: white;"><%= avcar.getCarname() %></td>
 <td style="color: white;"><%= avcar.getCarnumber() %></td>
 <td style="color: white;"><%= avcar.getPrice() %></td>
 <td><input type="hidden" name="hiddencategory" value="<%out.print(avcar.getCarcategory()); %>>" id="hiddencategory"></td>
     <td><input type="hidden" name="hiddencarname" value="<%out.print(avcar.getCarname());%>" id="hiddencarname" ></td>
     <td><input type="hidden" name="hiddencarnumber" value="<%out.print(avcar.getCarnumber());%>" id="hiddencarnumber" ></td>
     <td><input type="hidden" name="hiddencarprice" value="<%out.print(avcar.getPrice());%>" id="hiddencarprice" ></td>
 
 </tr>
 <%  }%>  
 
    	  
</tbody>
</table>
<%}}%>
 
 <div id="div2">
<input type="button" name="button" value="book" id="booked"/>

</div>
 
 <p id="adddata" style="text-align:center;color: white; "></p>
 

 
</body>
</html>