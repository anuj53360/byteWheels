package com.bytewheels.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.bytewheels.bean.AvailableCar;
import com.bytewheels.bean.BookCar;
import com.bytewheels.bean.LoginBean;
import com.bytewheels.bean.UserBean;
import com.bytewheels.conn.MySqlUtility;



public class DAO {

	
	public static int createUser(UserBean userbean){
		
		Connection conn = null;
		PreparedStatement pstm = null;
		int i=0;
		
		try{
		conn=MySqlUtility.getConnection();
		System.out.println("QQQQQQQ");
		String sql = "Insert into  bytewheels.user(firstname,lastname,mobile,emailid,password) values (?,?,?,?,?)";
		 
        pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, userbean.getFirstname());
        pstm.setString(2, userbean.getLastname());
        pstm.setString(3, userbean.getMobile());
        pstm.setString(4, userbean.getEmailid());
        pstm.setString(5, userbean.getPassword());
       
      i= pstm.executeUpdate();
        conn.close();
       
        
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		
		e.printStackTrace();
		
	}finally{
		MySqlUtility.pstClose(pstm);
		MySqlUtility.conClose(conn);
	}
		
		return i;
	}
	
	
	public static  LoginBean login(LoginBean loginbean){
		
		Connection conn = null;
		PreparedStatement pstm = null;
		int i=0;
		
		try{
		conn=MySqlUtility.getConnection();
		System.out.println("QQQQQQQ");
		
		String query="SELECT * from bytewheels.user where emailid=? and password=?";
		pstm = conn.prepareStatement(query);
		pstm.setString(1,loginbean.getEmailid());
		pstm.setString(2,loginbean.getPassword());
		
	ResultSet rs=pstm.executeQuery();
	if(rs.next()){
		
		loginbean.setEmailid(rs.getString("emailid"));
		loginbean.setPassword(rs.getString("password"));
		return loginbean;
		}	
	else {
		MySqlUtility.rsClose(rs);
    	return null;
	}
	}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	
		finally{
			MySqlUtility.pstClose(pstm);
			MySqlUtility.conClose(conn);
		}
	}
	
	public static List viewAvailabeCar(){
		List list=new ArrayList();
		Connection conn=MySqlUtility.getConnection();
		String query="SELECT carcategory,carname,carnumber,price from bytewheels.car where "
				+ " carnumber NOT IN(SELECT carnumber from bytewheels.booking where status=?)  ";
		
		try {
			PreparedStatement prs=conn.prepareStatement(query);
			prs.setString(1, "Y");
			ResultSet rs=prs.executeQuery();	
			while(rs.next()){
				AvailableCar availableca=new AvailableCar();
				availableca.setCarcategory(rs.getString("carcategory"));
				availableca.setCarname(rs.getString("carname"));
				availableca.setCarnumber(rs.getString("carnumber"));
				availableca.setPrice(rs.getString("price"));
				
				list.add(availableca);
				}
			
			if(list.size() > 0){
				System.out.println(list);
				return list;
				
			}else {
				return null;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
		}
	
	public static int bookCar(BookCar bookcar){
		Connection conn = null;
		PreparedStatement pstm = null;
		int i=0;
		
		String s="Y";
		
		
		try{
		conn=MySqlUtility.getConnection();
		System.out.println("QQQQQQQ");
		String sql = "Insert into  bytewheels.booking(startdate,enddate,carnumber,emailid,status) values (?,?,?,?,?)";
		 
        pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, bookcar.getStatingdate());
        pstm.setString(2, bookcar.getEndingdate());
        pstm.setString(3, bookcar.getCarNumber());
        pstm.setString(4, bookcar.getEmailid());
        pstm.setString(5, s);
       
       
      i= pstm.executeUpdate();
        conn.close();
       
        
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		
		e.printStackTrace();
		
	}finally{
		MySqlUtility.pstClose(pstm);
		MySqlUtility.conClose(conn);
	}
		
		return i;
}
	
}
