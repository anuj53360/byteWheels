package com.bytewheels.bean;

public class BookCar {
	
	private String status;
	
	private String carNumber;
	private String bookingid;
	private String emailid;
	private String statingdate;
	private String endingdate;
	
	public BookCar(){
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getBookingid() {
		return bookingid;
	}

	public void setBookingid(String bookingid) {
		this.bookingid = bookingid;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getStatingdate() {
		return statingdate;
	}

	public void setStatingdate(String statingdate) {
		this.statingdate = statingdate;
	}

	public String getEndingdate() {
		return endingdate;
	}

	public void setEndingdate(String endingdate) {
		this.endingdate = endingdate;
	}

	@Override
	public String toString() {
		return "BookCar [status=" + status + ", carNumber=" + carNumber + ", bookingid=" + bookingid + ", emailid="
				+ emailid + ", statingdate=" + statingdate + ", endingdate=" + endingdate + "]";
	}
	

}
