package com.bytewheels.bean;

public class AvailableCar {
	
	private String carcategory;
	private String carnumber;
	private String carname;
	private String price;
	private String startingdate;
	private String endingdate;
	
	public AvailableCar(){
		
	}

	public String getCarcategory() {
		return carcategory;
	}

	public void setCarcategory(String carcategory) {
		this.carcategory = carcategory;
	}

	public String getCarnumber() {
		return carnumber;
	}

	public void setCarnumber(String carnumber) {
		this.carnumber = carnumber;
	}

	public String getCarname() {
		return carname;
	}

	public void setCarname(String carname) {
		this.carname = carname;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getStartingdate() {
		return startingdate;
	}

	public void setStartingdate(String startingdate) {
		this.startingdate = startingdate;
	}

	public String getEndingdate() {
		return endingdate;
	}

	public void setEndingdate(String endingdate) {
		this.endingdate = endingdate;
	}

	@Override
	public String toString() {
		return "AvailableCar [carcategory=" + carcategory + ", carnumber=" + carnumber + ", carname=" + carname
				+ ", price=" + price + ", startingdate=" + startingdate + ", endingdate=" + endingdate + "]";
	}

	
	
}
