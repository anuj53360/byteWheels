package com.bytewheels.bean;

public class LoginBean {
	
	private String emailid;
	private String password;
	
	public LoginBean(){
		
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
