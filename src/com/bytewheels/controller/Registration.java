package com.bytewheels.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bytewheels.bean.UserBean;
import com.bytewheels.dao.DAO;


@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
                String firstname=request.getParameter("firstname");		
                String lastname=request.getParameter("lastname");	
                String emailid=request.getParameter("emailid");	
                String mobile=request.getParameter("mobile");	
                String password=request.getParameter("password");	
                
                UserBean userbean=new UserBean();
                userbean.setFirstname(firstname);
                userbean.setLastname(lastname);
                userbean.setEmailid(emailid);
                userbean.setMobile(mobile);
                userbean.setPassword(password);
               
             int i= DAO.createUser(userbean);
             if(i>0){
            	 request.setAttribute("sucessmsg","Registration is sucessfully done");
             }else{
            	 request.setAttribute("errormsg","Some Problem Happen on Registration");
             }
             
             RequestDispatcher rd=request.getRequestDispatcher("register.jsp");
             rd.forward(request, response);
		
	}

}
