package com.bytewheels.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bytewheels.bean.AvailableCar;
import com.bytewheels.dao.DAO;

import java.util.*;
import java.text.*;


@WebServlet("/Booking")
public class Booking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
		String startingdate=request.getParameter("startingdate");
		String endingdate=request.getParameter("endingdate");
		
		HttpSession session=request.getSession();
		System.out.println(startingdate+"startingdate");
		System.out.println(endingdate+"endingdate");
		long longstatingdate=0;
		long longendingdate=0;
		String url="";
		try {
			  DateFormat formatter ; 
			  Date date ; 
			  Date date1 ; 
			  formatter = new SimpleDateFormat("yy-mm-dd");
			  date = (Date)formatter.parse(startingdate); 
			  date1 = (Date)formatter.parse(endingdate); 
			  longstatingdate=date.getTime();
			  longendingdate=date1.getTime();
			  System.out.println(" is " +longstatingdate );
			  System.out.println(" is " +longendingdate );
			  }
		 catch (ParseException e){
			  System.out.println("Exception :"+e); 
			  }
		String startdate=String.valueOf(longstatingdate);
		String enddate=String.valueOf(longendingdate);
		session.setAttribute("startdate",startdate);
		session.setAttribute("endtdate",enddate);
		List l=DAO.viewAvailabeCar();
	
		request.setAttribute("availablecar", l);
		request.setAttribute("inpage", "available");
		url="booking.jsp";
		
		RequestDispatcher rd=request.getRequestDispatcher(url);
		rd.forward(request, response);
		
		
		
		
	}

}
