package com.bytewheels.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bytewheels.bean.LoginBean;
import com.bytewheels.dao.DAO;


@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println();
		String emailid=request.getParameter("emailid");
		String password=request.getParameter("password");
		
		LoginBean loginbean=new LoginBean();
		loginbean.setEmailid(emailid);
		loginbean.setPassword(password);
		
		String url="";
	LoginBean login=DAO.login(loginbean);
	if(login!=null){
	HttpSession session=request.getSession();
	session.setAttribute("email", emailid);
	System.out.println("if block is called");
	url="booking.jsp";
	}
	else{
		request.setAttribute("errormsg", "Please Check your maili'd");
		url="login.jsp";
	}
	RequestDispatcher rd=request.getRequestDispatcher(url);
	rd.forward(request, response);
		
	}

}
